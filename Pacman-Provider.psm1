function Get-PackageProviderName {
    return "pacman"
}

function Initialize-Provider {

}

function Get-Feature {
    Write-Output (New-Feature -Name "file-extensions" -Value @(".pkg.tar.xz"))
}
function Find-Package {
    param(
        [string] $name,
        [string] $requiredVersion,
        [string] $minimumVersion,
        [string] $maximumVersion
    )

    $r = pacman -Sii $name `
        | Where-Object {$_ -and $_[0] -ne " "} `
        | ForEach-Object {$_ -replace " +"," "} `
        | ForEach-Object {$hm = @{}} {$hm.Add($_.Split(":",2)[0].Trim(), $_.Split(":",2)[1].Trim())} {$hm}

    if (-not $?) {
        return
    }

    $swidObject = @{
        FastPackageReference = "$($r.Repository)/$($r.Name)/$($r.Version)";
        Name = $r.Name;
        Version = $r.Version;
        versionScheme  = "MultiPartNumeric";
        summary = $r.Desctiption;
        Source = $r.Repository;
    }

    $sid = New-SoftwareIdentity @swidObject
    Write-Output -InputObject $sid
}

function Get-InstalledPackage
{
    [CmdletBinding()]
    param
    (
        [string] $Name,
        [Version] $RequiredVersion,
        [Version] $MinimumVersion,
        [Version] $MaximumVersion
    )

    $null = pacman -Qi $Name

    if (-not $?) {
        return
    }

    Find-Package $Name
}

# function Download-Package {
#     [CmdletBinding()]
#     param
#     (
#         [Parameter(Mandatory=$true)]
#         [ValidateNotNullOrEmpty()]
#         [string]
#         $FastPackageReference,

#         [Parameter(Mandatory=$true)]
#         [ValidateNotNullOrEmpty()]
#         [string]
#         $Location
#     )

#     $repo, $pkg, $ver = $FastPackageReference.Split("/")
#     Invoke-WebRequest "https://www.archlinux.org/packages/$repo/x86_64/$pkg/download" -OutFile $Location
# }

function Install-Package {
    param(
        [string] $FastPackageReference
    )

    $repo, $pkg, $ver = $FastPackageReference.Split("/")
    pacman -S --noconfirm "$repo/$pkg=$ver"
}

function Uninstall-Package {
    param(
        [string] $fastPackageReference
    )

    $repo, $pkg, $ver = $FastPackageReference.Split("/")
    pacman -R --noconfirm "$pkg"
}