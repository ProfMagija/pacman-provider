@{
    RootModule = 'Pacman-Provider.psm1'
    ModuleVersion = '0.0.1'
    GUID = 'c6b57fef-f55c-44fd-bafc-17cbad58d630'
    Author = 'Nikola Bebić'
    Copyright = '© Nikola Bebić. All rights reserved.'
    Description = 'Pacman provider enables installation of pacman packages using OneGet'
    PowerShellVersion = '3.0'
    FunctionsToExport = @()
    RequiredModules = @('PackageManagement')
    PrivateData = @{
        "PackageManagementProviders" = 'Pacman-Provider.psm1'
        PSData = @{
            # Tags applied to this module to indicate this is a PackageManagement Provider.
            Tags = @("PackageManagement","Provider")
        } # End of PSData
    }
}
